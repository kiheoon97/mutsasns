package com.example.mutsasns.controller;

import com.example.mutsasns.domain.dto.UserJoinReq;
import com.example.mutsasns.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(UserControllerTest.class)
public class UserControllerTest {
    @Autowired
    MockMvc mockMvc;
    @MockBean
    UserService userService;
    @Autowired
    ObjectMapper objectMapper;

    @Test
    @DisplayName("회원가입 Success")
    @WithMockUser
    void joinSuccess()throws Exception{
        UserJoinReq userJoinReq = UserJoinReq.builder()
                .userName("user")
                .password("password")
                .build();
    }
}
