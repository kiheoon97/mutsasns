package com.example.mutsasns.service;

import com.example.mutsasns.domain.dto.UserJoinReq;
import com.example.mutsasns.domain.entity.UserEntity;
import com.example.mutsasns.exception.AppException;
import com.example.mutsasns.exception.ErrorCode;
import com.example.mutsasns.repository.UserRepository;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;


@Service
@Getter
@Setter
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    @Value("${jwt.token.secret}")
    private String secretKey;

    public UserEntity join(UserJoinReq userJoinReq) {
        //회원 중복 확인
        userRepository.findByUserName(userJoinReq.getUserName())
                .ifPresent(user -> {
                    throw new AppException(ErrorCode.DUPLICATED_USER_NAME, String.format(" %s 는 이미 있습니다.",userJoinReq.getUserName()));
                });
        UserEntity userEntity = UserEntity.of(userJoinReq.getUserName(),
                bCryptPasswordEncoder.encode(userJoinReq.getPassword()));
        UserEntity savedUser = userRepository.save(userEntity);
        return savedUser;
    }

    public User loadUserByUsername(String userName) throws UsernameNotFoundException{
        UserEntity userEntity = userRepository.findByUserName(userName)

    }

    public String login(String userName, String password) {
        User savedUser = load
    }
}
