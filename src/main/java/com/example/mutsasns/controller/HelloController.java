package com.example.mutsasns.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/hello")
public class HelloController {
    @GetMapping
    public ResponseEntity<String> hello(){
        return ResponseEntity.ok().body("hello");
    }
    @GetMapping("/test")
    public ResponseEntity<String> Test(){
        return ResponseEntity.ok().body("11");
    }
}
