package com.example.mutsasns.controller;

import com.example.mutsasns.domain.dto.UserJoinReq;
import com.example.mutsasns.domain.entity.UserEntity;
import com.example.mutsasns.domain.response.Response;
import com.example.mutsasns.domain.response.UserJoinRes;
import com.example.mutsasns.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/users")
public class UserController {
    private final UserService userService;

    @PostMapping("/join")
    public Response<UserJoinRes> joinResponse(@RequestBody UserJoinReq userJoinReq){
        UserEntity userEntity = userService.join(userJoinReq);
        UserJoinRes userJoinRes = UserJoinRes.builder()
                .userId((userEntity.getId()))
                .userName(userEntity.getUserName())
                .build();
        return Response.success(userJoinRes);

    }
}
