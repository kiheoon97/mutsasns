package com.example.mutsasns.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class AppException extends RuntimeException {
    private ErrorCode errorCode;
    private String message = null;

    public AppException(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }

    @Override
    public String getMessage() {
        if (message == null) return errorCode.getErrorMessage();
        return String.format("%s. %s", errorCode.getErrorMessage(), message);
    }
}
