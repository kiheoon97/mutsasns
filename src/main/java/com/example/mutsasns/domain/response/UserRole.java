package com.example.mutsasns.domain.response;

public enum UserRole {
    USER,
    ADMIN
}
