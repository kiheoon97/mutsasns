package com.example.mutsasns.domain.entity;

import com.example.mutsasns.domain.response.UserRole;
import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class UserEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private Integer id;
    private String userName;
    private String password;
    private Timestamp registeredAt;
    private Timestamp removedAt;
    private Timestamp updatedAt;
    private UserRole role=UserRole.USER;

    public static UserEntity of(String userName, String encodePassword){
        UserEntity userEntity = new UserEntity();
        userEntity.setUserName(userName);
        userEntity.setPassword(encodePassword);
        return userEntity;
    }

}
